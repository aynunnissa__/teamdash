package propensid03.teamdash.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "target")
public class TargetModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idTarget;

    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = "YYYY-MM")
    private LocalDate targetMonth;

    @NotNull
    @Column(name="jumlah_target", nullable = false)
    private int jumlahTarget;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ProjectModel project;
}
