package propensid03.teamdash.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import propensid03.teamdash.model.RoleModel;
import propensid03.teamdash.model.UserModel;
import propensid03.teamdash.service.RoleService;
import propensid03.teamdash.service.UserService;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @GetMapping("/viewall")
    public String listUser(Model model){

        List<UserModel> listUser = userService.getListOfUser();

        model.addAttribute("listUser", listUser);
        model.addAttribute("activePage", "viewallUser");

        //Return view template yang diinginkan
        return "viewall-user";
    }

    @GetMapping("/add")
    private String addUserFormPage(Model model){
        UserModel user = new UserModel();
        List<RoleModel> listRole = roleService.getListRole();
        model.addAttribute("user", user);
        model.addAttribute("listRole", listRole);
        return "form-add-user";
    }

    @PostMapping(value = "/add")
    private String addUserSubmit(@ModelAttribute UserModel user, Model model){
        userService.addUser(user);
        model.addAttribute("user", user);
        return "redirect:/";
    }

    @GetMapping("/delete/{username}")
    public String deleteUserForm(@PathVariable String username, Model model) {
        UserModel delUser = userService.getUserByUsername(username);
        userService.deleteUser(delUser);

        model.addAttribute("delUser", delUser);
        return "delete-user";
    }

    @GetMapping(value = "/update-password/{username}")
    public String formUpdatePassword(
        @PathVariable String username,
        Model model
    ){
        UserModel user = userService.getUserByUsername(username);
        model.addAttribute("user", user);
        model.addAttribute("text", "");
        return "form-update-password";
    }

    @PostMapping(value = "/update-password")
    public String submitUpdatePassword(@RequestParam String oldPass, String newPass, String confirmedPass, String username, Model model){
        UserModel user = userService.getUserByUsername(username);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if(passwordEncoder.matches(oldPass, user.getPassword())){
            if(newPass.equals(confirmedPass)){
                userService.updatePassword(user, newPass);
                model.addAttribute("text","Password succesfully changed");
            }
            else{
                model.addAttribute("text", "Password doesn't match");
            }
        }
        else{
            model.addAttribute("text", "Password is wrong");
        }
        return "update-password";
    }

}
