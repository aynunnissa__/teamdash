package propensid03.teamdash.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import propensid03.teamdash.model.UserModel;
import propensid03.teamdash.service.UserRestService;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/v1")
public class UserRestController {
    @Autowired
    private UserRestService userRestService;

    @PostMapping(value = "/user")
    private ResponseEntity createUser(@Valid @RequestBody UserModel user, BindingResult bindingResult){
        if(bindingResult.hasFieldErrors()) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Request body has invalid type or missing field"
            );
        }else{
            userRestService.createUser(user);
            return ResponseEntity.ok("Create user success");
        }
    }

    @GetMapping(value = "/user/{idUser}")
    private UserModel retrieveUser(@PathVariable(value = "idUser") Long idUser){
        try{
            return userRestService.getUserByIdUser(idUser);
        }
        catch (NoSuchElementException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Id User " + String.valueOf(idUser) + "Not Found."
            );
        }
    }

    @DeleteMapping(value = "/user/{idUser}")
    private ResponseEntity deleteUser(
            @PathVariable("idUser") Long idUser
    ){
        try {
            userRestService.deleteUser(idUser);
            return ResponseEntity.ok("User has been deleted!");
        } catch (NoSuchElementException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "User with Id User " + String.valueOf(idUser) + " Not Found."
            );
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "User can't be deleted"
            );
        }
    }

    @GetMapping(value = "/list-user")
    private List<UserModel> retrieveListUser(){
        return userRestService.retrieveListUser();
    }

}
