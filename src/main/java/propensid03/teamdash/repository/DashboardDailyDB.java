package propensid03.teamdash.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import propensid03.teamdash.model.DashboardDailyModel;

public interface DashboardDailyDB extends JpaRepository<DashboardDailyModel, Long> {
}
