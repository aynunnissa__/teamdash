package propensid03.teamdash.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import propensid03.teamdash.model.DashboardMonthlyModel;

public interface DashboardMonthlyDB extends JpaRepository<DashboardMonthlyModel, Long>{
}
