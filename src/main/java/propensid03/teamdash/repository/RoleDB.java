package propensid03.teamdash.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import propensid03.teamdash.model.RoleModel;

public interface RoleDB extends JpaRepository<RoleModel, Long> {
}
