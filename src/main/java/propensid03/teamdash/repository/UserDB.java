package propensid03.teamdash.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import propensid03.teamdash.model.UserModel;

import java.util.Optional;

public interface UserDB extends JpaRepository<UserModel, Long>{
    UserModel findByUsername(String username);
    Optional<UserModel> findByIdUser(Long idUser);
}

