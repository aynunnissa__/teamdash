package propensid03.teamdash.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO {

    @JsonProperty
    private Long idUser;

    @JsonProperty
    public String nama;

    @JsonProperty
    public String username;

    @JsonProperty
    public String password;

    @JsonProperty
    public Integer jenisKelamin;
}






