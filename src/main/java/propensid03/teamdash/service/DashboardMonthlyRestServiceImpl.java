package propensid03.teamdash.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import propensid03.teamdash.model.DashboardMonthlyModel;
import propensid03.teamdash.repository.DashboardMonthlyDB;
import propensid03.teamdash.rest.Setting;

import javax.transaction.Transactional;

@Service
@Transactional
public class DashboardMonthlyRestServiceImpl implements DashboardMonthlyRestService {
    private final WebClient webClient;

    @Autowired
    private DashboardMonthlyDB dashboardMonthlyDB;

    public DashboardMonthlyRestServiceImpl(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl(Setting.dasboardMonthlyUrl).build();
    }

    @Override
    public DashboardMonthlyModel getDataMonthly(){
        return null;
    }
}
