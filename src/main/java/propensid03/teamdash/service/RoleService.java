package propensid03.teamdash.service;

import propensid03.teamdash.model.RoleModel;

import java.util.List;

public interface RoleService {
    List<RoleModel> getListRole();
}
