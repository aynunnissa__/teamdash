package propensid03.teamdash.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import propensid03.teamdash.model.UserModel;
import propensid03.teamdash.repository.UserDB;

import javax.transaction.Transactional;
import java.lang.reflect.Array;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDB userDB;

    @Override
    public UserModel addUser(UserModel user) {
        String pass = encrypt(user.getPassword());
        user.setPassword(pass);
        return userDB.save(user);
    }

    @Override
    public String encrypt(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        return hashedPassword;
    }

    @Override
    public List<UserModel> getListOfUser() {
        return userDB.findAll();
    }

    @Override
    public void deleteUser(UserModel user) { userDB.delete(user); }

    @Override
    public UserModel getUserByUsername(String username) {
        UserModel user = userDB.findByUsername(username);
        return user;
    }

    @Override
    public void updatePassword(UserModel user, String newPass) {
        UserModel userNow = userDB.findByUsername(user.getUsername());
        user.setPassword(encrypt(newPass));
        userDB.save(user);
    }
}
