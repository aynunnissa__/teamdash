package propensid03.teamdash.service;

import propensid03.teamdash.model.DashboardMonthlyModel;

public interface DashboardMonthlyRestService {
    DashboardMonthlyModel getDataMonthly();
}
