package propensid03.teamdash.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import propensid03.teamdash.model.UserModel;
import propensid03.teamdash.repository.UserDB;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class UserRestServiceImpl implements UserRestService{
    private final WebClient webClient;

    @Autowired
    private UserDB userDB;

    @Override
    public UserModel createUser(UserModel user){
        return userDB.save(user);
    }

    @Override
    public List<UserModel> retrieveListUser(){
        return userDB.findAll();
    }

    @Override
    public UserModel getUserByIdUser(Long idUser){
        Optional<UserModel> user = userDB.findByIdUser(idUser);

        if(user.isPresent()){
            return user.get();
        }else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void deleteUser(Long idUser) {
        UserModel user = getUserByIdUser(idUser);
        userDB.delete(user);
    }

    public UserRestServiceImpl(WebClient.Builder webClientBuilder){
        this.webClient = webClientBuilder.baseUrl("https://api.agify.io").build();
    }
}
