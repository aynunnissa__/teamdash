package propensid03.teamdash.service;

import propensid03.teamdash.model.UserModel;
import reactor.core.publisher.Mono;

import java.util.List;

public interface UserRestService {
    UserModel createUser(UserModel user);
    List<UserModel> retrieveListUser();
    UserModel getUserByIdUser(Long idUser);
    void deleteUser(Long idUser);
}
