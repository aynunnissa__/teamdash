package propensid03.teamdash.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import propensid03.teamdash.model.DashboardDailyModel;
import propensid03.teamdash.repository.DashboardDailyDB;
import propensid03.teamdash.rest.Setting;

import javax.transaction.Transactional;

@Service
@Transactional
public class DashboardDailyRestServiceImpl implements DashboardDailyRestService {
    private final WebClient webClient;

    @Autowired
    private DashboardDailyDB dashboardDailyDB;

    public DashboardDailyRestServiceImpl(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl(Setting.dasboardDailyUrl).build();
    }

    @Override
    public DashboardDailyModel getDataDaily() {
        return null;
    }
}
