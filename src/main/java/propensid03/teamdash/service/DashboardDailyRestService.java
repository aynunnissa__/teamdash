package propensid03.teamdash.service;

import propensid03.teamdash.model.DashboardDailyModel;

public interface DashboardDailyRestService {
    DashboardDailyModel getDataDaily();
}
