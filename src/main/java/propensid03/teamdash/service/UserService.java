package propensid03.teamdash.service;

import propensid03.teamdash.model.UserModel;

import java.util.List;

public interface UserService {
    UserModel addUser(UserModel user);
    String encrypt(String password);
    List<UserModel> getListOfUser();
    void deleteUser(UserModel user);
    UserModel getUserByUsername(String username);
    void updatePassword(UserModel user, String newPass);
}
